module.exports = function(grunt) {

    require('time-grunt')(grunt);

    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'static/scss',
                    src: ['*.scss'],
                    dest: 'static/css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['static/scss/*.scss'],
            tasks: ['css'],
        },

        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        'static/css/*.css',
                        '*.html',
                        'static/js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'static/images/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './', //current working directory
                    src: ['*.html'],
                    dest: 'dist/'
                }]
            },
            fonts:{
                files:[{
                expand:true,
                    dot:true,
                    cwd:'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest:'dist/'
                }]
            }
        },

        clean: {
            build: {
                src: ['dist/'] //se limpia el directorio de distucción
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerev: {
            options: {
                algorithm: 'md5',
                length: 15
            },
            files: {
                src: ['dist/static/css/*.css', 'dist/static/js/*.js']
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html','juegos.html','acerca.html','registro.html','contacto.html','clasificacion.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html','dist/juegos.html','dist/acerca.html','dist/registro.html','dist/contacto.html','dist/clasificacion.html'],
            options: {
                assetsDir: ['dist/', 'dist/static/css', 'dist/static/js']
            }
        }

    });

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean', //Borramos el contenido de dist
        'copy', //Copiamos los archivos html a dist
        'imagemin', //Optimizamos imagenes y las copiamos a dist
        'useminPrepare', //Preparamos la configuracion de usemin
        'concat',
        'cssmin',
        'uglify',
        'filerev', //Agregamos cadena aleatoria
        'usemin' //Reemplazamos las referencias por los archivos generados por filerev
    ]);

};
