$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$(function () {
    $('[data-toggle="popover"]').popover()
})

$(function () {
    $('.carousel').carousel({
        interval: 3000
    })
})

$(function () {
    $('#suscripcionModal').on('show.bs.modal', function (e) {
        console.log("Se está mostrando el modal de suscripción");
        // se cambia el color del botón y se desactiva
        $('#suscripcion').css('background-color','red');
        $('#suscripcion').css('color','blue');
        $('#suscripcion').attr("disabled", true);
    })

    $('#loginModal').on('show.bs.modal', function (e) {
        console.log("Se está mostrando el modal del login");
        // se cambia el color del enlace y se desactiva
        $('#login').addClass(' active');
        $('#ingresar').attr("disabled", true);
    })

    $('#suscripcionModal').on('shown.bs.modal', function (e) {
        console.log("Se ha mostrado el modal de suscripción");
    })

    $('#loginModal').on('shown.bs.modal', function (e) {
        console.log("Se ha mostrado el modal del login");
    })

    $('#suscripcionModal').on('hidde.bs.modal', function (e) {
        console.log("Se está cerrando el modal de suscripción");
    })

    $('#loginModal').on('hidde.bs.modal', function (e) {
        console.log("Se está cerrando el modal de login");
    })

    $('#suscripcionModal').on('hidden.bs.modal', function (e) {
        console.log("Se ha cerrado el modal de suscripción");
        // se cambia al color original
        $('#suscripcion').css('background-color','blue');
        $('#suscripcion').css('color','orange');
        $('#suscripcion').attr("disabled", false);
    })

    $('#loginModal').on('show.bs.modal', function (e) {
        console.log("Se ha cerrado el modal del login");
        // se cambia el color del enlace
        $('#login').removeClass(' active');
        $('#ingresar').attr("disabled", false);
    })

})
